require('./monitoring.styl');

'use strict';

var timeGetServer = 0;
var timerGetServer = 0;
var flagTimerRepeat = 0;
jQuery().ready(function () {
    var btnSubmit = document.getElementById('getsrv');
    var sendReq = function (event) {
        var updateTime = document.querySelector('#tmup');
        var depthScan = document.querySelector('#depth');
        var updateTimeValue = updateTime.value;
        var depthScanValue = depthScan.value;

        var srvlst = document.querySelector('#srvlst');
        var h2srvlst = document.querySelector('#h2srvlst');

        clearTimeout(timerGetServer);
        timerGetServer = null;

        if (!depthScanValue) {
            var msg = 'Не указан один из параметров. Пожалуйста задайте глубину сканирования.';
            h2srvlst.innerHTML = msg;
            clearData()
            return;
        }

        var depthScanValueNum = Number(depthScanValue);
        if (isNaN(depthScanValueNum)) {
            var msg = 'Глубина сканирования указана не верно. Значение должно быть числом.';
            h2srvlst.innerHTML = msg;
            clearData()
            return;
        }

        if (depthScanValueNum < 1) {
            var msg = 'Глубина сканирования указано не верно. Значение должно быть положительным числом.';
            h2srvlst.innerHTML = msg;
            clearData()
            return;
        }

        if (updateTimeValue) {
            flagTimerRepeat = 0;
            var updateTimeValueNum = Number(updateTimeValue);
            if (isNaN(updateTimeValueNum)) {
                var msg = 'Время обновления указано не верно. Значение должно быть числом.';
                h2srvlst.innerHTML = msg;
                clearData()
                return;
            }

            if (updateTimeValueNum < 0) {
                var msg = 'Время обновления указано не верно. Значение должно не отрицательным числом.';
                h2srvlst.innerHTML = msg;
                clearData()
                return;
            }

            var updateTimeValueNumMin = 10;
            if (updateTimeValueNum < updateTimeValueNumMin) {
                var msg = 'Время обновления указано не верно. Значение должно быть не менее ' + updateTimeValueNumMin + ' секунд.';
                h2srvlst.innerHTML = msg;
                clearData()
                return;
            }
            flagTimerRepeat = 1
            timeGetServer = updateTimeValue;
        }
        else {
            flagTimerRepeat = 0
        }


        getListServers(depthScanValue);
        function getListServers(depthScanValue) {
            $.ajax({
                url: "getServer",
                method: 'GET',
                data: {d: depthScanValue}
            }).done(function (datareq) {
                srvlst.innerHTML = '';
                if (datareq['error']) {
                    h2srvlst.innerHTML = datareq['error'];
                }
                else {
                    if (datareq['servers']) {
                        var listLi = '';
                        var servers = datareq['servers']
                        servers.forEach(function (item, i, arr) {
                            listLi += '<li class="list-group-item">' + item + '</li>';
                        });
                        if (listLi) {
                            h2srvlst.innerHTML = 'Доступные сервера:';
                            srvlst.innerHTML = listLi;
                        }
                        else {
                            h2srvlst.innerHTML = 'По заданым критериям поиска нет доступных серверов.';
                        }
                    }
                }
                clearTimeout(timerGetServer);
                timerGetServer = null;
                if (flagTimerRepeat === 1){
                    timerGetServer = setTimeout(function name() {
                        getListServers(depthScanValue);
                    }, timeGetServer * 1000)
                };

            }).fail(function (datareq) {
                clearTimeout(timerGetServer);
                timerGetServer = null;
                h2srvlst.innerHTML = 'Возникли проблемы при получение запроса. Повтор.';
                srvlst.innerHTML = '';
                if (flagTimerRepeat === 1) {
                    timerGetServer = setTimeout(function name() {
                        getListServers(depthScanValue);
                    }, timeGetServer * 1000);
                }
            });
        };

        function clearData() {
            clearTimeout(timerGetServer);
            timerGetServer = null;
            srvlst.innerHTML = '';
        }
    }

    btnSubmit.removeEventListener('click', sendReq, false);
    btnSubmit.addEventListener('click', sendReq, false);
});


