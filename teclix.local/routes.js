/**
 * Created by isukhachev on 24.04.17.
 */
'use strict';

const pages = require('./controllers/pages');

module.exports = function (app, passport) {
    app.get('/', setLoggedFlag, pages.index);

    app.get('/monitoring', setLoggedFlag, pages.monitoring);

    app.get('/getServer', pages.getServers);

    app.all('*', pages.error404);

    app.use((err, req, res) => {
        console.error(err);
        res.sendStatus(500);
    });
}
function setLoggedFlag(req, res, next) {
    next();
}