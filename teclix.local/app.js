'use strict';
var jsport = 8888;

const path = require('path');

const express = require('express');
const app = express();

const hbs = require('hbs');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const viewsDir = path.join(__dirname, 'bundles');
const publicDir = path.join(__dirname, 'public');

app.set('views', viewsDir);
app.set('view engine', 'hbs');
hbs.registerPartials(path.join(__dirname, '/views/partials'));

app.use(morgan('dev'));
app.use(require('cookie-parser')());

app.use(bodyParser.json({limit: '200mb'}));
app.use(bodyParser.urlencoded({extended: true}));

app.use(require('compression')());

app.use(express.static(publicDir, {
    maxAge: 1000 * 60 * 60 * 24 * 30
}));

app.set('port', (process.env.PORT || jsport));

app.use((err, req, res, next) => {
    console.error(err);
    next();
});

app.use((req, res, next) => {
    req.commonData = {
        meta: {
            description: 'Awesome notes',
            charset: 'utf-8'
        },
        page: {
            title: 'Awesome notes'
        },
        isDev: process.env.NODE_ENV === 'development',
        title: 'TecLix'
    };
    next();
});

require('./routes')(app);

app.listen(app.get('port'),
    () => console.log(`Listening on port ${app.get('port')}`));

module.exports = app;
