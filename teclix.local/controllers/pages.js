/**
 * Created by isukhachev on 25.04.17.
 */
'use strict';

var sumFiles = 0;
var sumMaxFiles = 0;
var resItems = [];
const fs = require('fs');
const handlebars = require('hbs').handlebars;
const layouts = require('handlebars-layouts');
const util = require('util');

handlebars.registerHelper(layouts(handlebars));
handlebars.registerPartial('base', fs.readFileSync('./views/base.hbs', 'utf8'));

exports.index = (req, res) => {
    var template = handlebars.compile(fs.readFileSync('./views/main.hbs', 'utf8'));
    res.send(template(Object.assign({
        title: 'TecLix Handler',
        items: []
    }, req.commonData)));
};

exports.monitoring = (req, res) => {
    var template = handlebars.compile(fs.readFileSync('./views/monitoring/monitoring.hbs', 'utf8'));
    res.send(template(Object.assign({
        title: 'TecLix Monitoring',
        items: []
    }, req.commonData)));
};


exports.getServers = (req, res, next) => {
    var depth = req.param('d');
    var path = require('path');
    if (!depth) {
        res.send({error: 'Не указан парметр глубины сканирования.'});
    }

    depth = depth * 60 * 1000;
    var dirPath = "/opt/ftp";
    //var dirPath = "C:\opt\ftp"; //TODO

    var fs = require('fs');
    fs.readdir(dirPath, function (err, items) {
        console.log(items);
        if (items) {
            var nowTime = new Date().getTime();
            sumMaxFiles = items.length;
            sumFiles = 0;
            resItems = [];
            console.log(sumFiles, sumMaxFiles);
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var itemTr = item;

                readNameFile(dirPath, itemTr, nowTime, depth, (result) => {
                    res.send({servers: result});
                });
            }
        }
        else {
            res.send({error: 'Доступные сервера не найдены'});
        }
    });
};

exports.error404 = (req, res) => {
    var data = {
        code: 404,
        error: 'not found'
    };
    res.render('error', Object.assign(req.commonData, data));
};

function readNameFile(dirPath, item, nowTime, depth, callbackF) {
    fs.stat(dirPath + "/" + item, function (err, stats) {
        sumFiles += 1;
        //fs.stat(dirPath + "\" + item, function(err, stats){ //TODO
        var mtime = new Date(util.inspect(stats.mtime)).getTime();
        if ((nowTime - mtime) < depth) {
            var newitem = item.replace(/(.+)(_[0-9]{6}_[0-9]{4}\.txt)/, "$1");
            if (resItems.indexOf(newitem) < 0) {
                resItems.push(newitem);
            }
        }
        if (sumFiles === sumMaxFiles) {
            callbackF(resItems);
        }
    });
}